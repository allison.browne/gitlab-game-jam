from ursina import *

app = Ursina()
player = Sprite("tblock")

enemies = [Sprite("tblock_enemy")]

SPEED = 2.0

def move_enemies():
    for e in enemies:
        print(e)

def update():
    player.x += held_keys['d'] * time.dt * SPEED
    player.x -= held_keys['a'] * time.dt * SPEED
    player.y += held_keys['w'] * time.dt * SPEED
    player.y -= held_keys['s'] * time.dt * SPEED
    move_enemies()

def input(key):
    if key == 'space':
        player.y += 1
        invoke(setattr, player, 'y', player.y-1, delay=.25)


# start running the game
app.run()
